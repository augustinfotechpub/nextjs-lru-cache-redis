const data = require('./301_data.json')
const isProd = process.env.NODE_ENV === 'production'
module.exports = {
   i18n: {
    locales: ['en'],
    defaultLocale: 'en',
    localeDetection: false,
  },
  build: {
    vendor: ['scrollmagic'],
    
  },
  plugins: [
    {
      src: '../plugins/scrollmagic' 
    }
  ],
  reactStrictMode: true,
  images: {
    domains: [
      's3.ap-south-1.amazonaws.com',
      'https://cloudfront.net'
    ],
    },
      headers:
    {
      'Content-Type':'application/json',
      'Access-Control-Allow-Credentials' : 'true',
      'Access-Control-Allow-Origin': 'True',
      'Access-Control-Allow-Methods': 'GET, POST, PATCH, DELETE, PUT, OPTIONS',
      'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
        },
  eslint: {
    ignoreDuringBuilds: true,
  },
  distDir: 'build',
  async redirects() {
    // return [
    //   process.env.MAINTENANCE_MODE === "1"
    //     ? { source: "/((?!maintenance).*)", destination: "/maintenance", permanent: false }
    //     : null,
    // ].filter(Boolean);
  
    let arr = []
    {data.data.map((redirect) => {
      arr.push(redirect)
    })
    }
  return arr
}
}
