import React,{Component} from 'react';
import _ from 'lodash'
import {Container} from 'react-bootstrap';
import dynamic from 'next/dynamic'
const SectionTitle  = dynamic(() =>  import('../HomepageSection/SectionInnerComponent/SectionTitle'))
const OffersSlider  = dynamic(() =>  import('./OffersSlider'))

export default class OffersSection extends Component {
    constructor(props) {
        super(props);
    }

    compareDate(val){

        return new Date(val).getTime() - new Date().getTime() > 0 ? true : false;
      }

    render(){
        this.props.data.attributes.Offers_Slider = this.props.data?.attributes?.Offers_Slider.filter((el) => this.compareDate(el.Offer_Valid_Upto_Date))


        this.props.data?.attributes?.Offers_Slider.sort((a,b)=>{
            if(a?.Offer_Valid_Upto_Date > b?.Offer_Valid_Upto_Date ) return -1;
            if(a?.Offer_Valid_Upto_Date  < b?.Offer_Valid_Upto_Date ) return 1;
             return 0
        })
        return (

            <>
            {
                this.props.data?.attributes?.Offers_Slider.length !== 0 &&
                <section className="section offers-slider-section" style={{backgroundColor:"#f0faf2"}}>
                <div className="section-inner">
                    <Container>
                        <div className="section-wrap">
                            <SectionTitle sectionTitle={this.props.data?.attributes?.Offer_Section_Title} titleTheme="dark-title mb-80"/>

                            
                            <div className='offers-slider-wrap'>
                                <OffersSlider data={this.props.data}/>
                            </div>
                        </div>
                    </Container>
                </div>
            </section>
            }
              
            </>
        )
    }
 
}

